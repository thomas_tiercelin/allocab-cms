<!DOCTYPE html>


<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->


<!--[if IE 7]>    <html class="ie ie7 oldie" lang="fr"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9 ie ie8" lang="fr"> <![endif]-->
<!--[if gt IE 8]> <html class="ie no-js" lang="fr"> <![endif]-->

<!--[if !IE]> -->  <html class="no-js" lang="fr"> <!-- <![endif]-->


<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<script>
	 _udn = "allocab.com"; 
	</script> 	
	<!-- Google Analytics Content Experiment code -->
	<script>function utmx_section(){}function utmx(){}(function(){var
	k='GA_AB_TEST_KEY',d=document,l=d.location,c=d.cookie;
	if(l.search.indexOf('utm_expid='+k)>0)return;
	function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
	indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
	length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
	'<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
	'://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
	'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
	valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
	'" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
	</script><script>utmx('url','A/B');</script>
	<!-- End of Google Analytics Content Experiment code -->	
	
	<meta charset="utf-8" />
	<meta name="language" content="fr-FR" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width, user-scalable = no, initial-scale=1, maximum-scale=1" />	
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">

	<link rel="apple-touch-startup-image" href="$ASSETS_BASEPATH$images/ios/splash.png">
	<link rel="apple-touch-icon" href="$ASSETS_BASEPATH$images/ios/touch-icon-iphone-precomposed.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="$ASSETS_BASEPATH$images/ios/touch-icon-ipad-precomposed.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="$ASSETS_BASEPATH$images/ios/touch-icon-iphone-retina-precomposed.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="$ASSETS_BASEPATH$images/ios/touch-icon-ipad-retina-precomposed.png" />
	
	<meta name="detectify-verification" content="74fa4520f4b6b30d2fce5e4b4d95e6c0" />

	<link rel="shortcut icon" type="image/png" href="$ASSETS_BASEPATH$images/illustr/favicon.png" />
	<title>$TITLE$</title>
	<meta name="description" content="$DESCRIPTION$" />

	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900' rel='stylesheet' type='text/css'>
	<!--[if lte IE 8]>
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300' rel='stylesheet' type='text/css'>		
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:600' rel='stylesheet' type='text/css'>		
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:900' rel='stylesheet' type='text/css'>		
	<![endif]-->	

	<!-- Included CSS Files -->
	<link rel="stylesheet" href="$ASSETS_BASEPATH$stylesheets/jquery-ui.css" media="screen, projector, print" rel="stylesheet" type="text/css" >
	<link rel="stylesheet" href="$ASSETS_BASEPATH$stylesheets/app.css" media="screen, projector, print" rel="stylesheet" type="text/css" >


	<script src="$ASSETS_BASEPATH$javascripts/foundation/foundation.min.js"></script>


  <script src="$ASSETS_BASEPATH$javascripts/script.js"></script>	
  
		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', '$GA_CODE$']);
		  _gaq.push(['_setDomainName', 'allocab.com']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

		</script>	

		<!-- START IADVIZE CHAT --> 
 		<script type='text/javascript'> 
 			(function() { 
     			var idz = document.createElement('script'); idz.type = 'text/javascript'; idz.async = true; 
     			idz.src = document.location.protocol + '/' + '/' + 'lc.iadvize.com/iadvize.js?sid=13457&lang=fr'; 
     			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(idz, s); 
 			})(); 
 		</script> 
 		<!-- END IADVIZE CHAT -->


<style>
ul#main-menu {
	padding-left:0px;
}
</style>

</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/fr_FR/all.js#xfbml=1&appId=226536317359126";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<div class="row twelve columns hide-for-small" id="trait">&nbsp;</div>

	<div class="show-for-small fixed contain-to-grid">
    <nav class="top-bar">
      <ul>
        <!-- Title Area -->
        <li class="name">
          <h1>
            <a href="/">Menu</a>       
          </h1>
        </li>
        <li class="toggle-topbar"><a href="#"></a></li>
      </ul>

      <section>
        <!-- Left Nav Section -->
        <ul class="left">
          <li class="divider"></li>

			$if(loggedIn)$				
				$if(ALLOCAB)$
					<li class="has-dropdown">
						<a href="javascript:void(0)">Utilisateurs</a>
						<ul class="dropdown">
							<li>
								<a href="/ac/driveraccount/index?sortOrder=DESC&sortBy=createdAt&filterBy=INSPECTION">Chauffeurs</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="/ac/passengeraccount/index">Passagers</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="/ac/contact/index">Contacts</a>
							</li>												
						</ul>
					</li>
			<li class="divider"></li>		
					<li class="has-dropdown">
						<a href="javascript:void(0)">Plateforme</a>
						<ul class="dropdown">
							<li>
								<a href="javascript:search();">Recherche</a>
							</li>								
							<li class="divider"></li>
							<li>
								<a href="/ac/page/index">CMS</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="/ac/tree/index">Arbres</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="/ac/shortener/index">Campagnes</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="/ac/application/index">Applications</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="/ac/fare/index?sortOrder=ASC&sortBy=datetime&filterBy=INCOMING">Réservations</a>
							</li>					
							<li class="divider"></li>						
							<li>
								<a href="/ac/invoice/index">Facturation</a>
							</li>	

							<li class="divider"></li>						
							<li>
								<a href="/ac/invoiceresume/index?sortOrder=DESC&sortBy=startDate">Etats chauffeurs</a>
							</li>								

							

						</ul>
					</li>
			<li class="divider"></li>	
					<li class="has-dropdown">
						<a href="javascript:void(0)">Véhicules</a>
						<ul class="dropdown">
							<li>
								<a href="/ac/vehicle/index">Tous les véhicules</a>
							</li>
							<li class="divider"></li>					
							<li>
								<a href="/ac/vehicletype/index">Types de véhicules</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="/ac/vehiclepanel/index">Gammes de véhicules</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="/ac/vehiclelicense/index">Licenses de véhicules</a>
							</li>							
						</ul>
					</li>
			<li class="divider"></li>		
					<li class="has-dropdown">
						<a href="javascript:void(0)">Documents</a>
						<ul class="dropdown">
							<li>
								<a href="/ac/document/index">Tous les documents</a>
							</li>				
							<li class="divider"></li>	
							<li>
								<a href="/ac/documenttype/index">Types de documents</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="/ac/documenttemplate/index">Modèles</a>
							</li>
						</ul>
					</li>																			
				$elseif(DRIVER)$					
					<li><a href="$DRIVER_ACCOUNT_INVOICE_RESUME$">Tableau de bord</a></li>	
					<li class="divider"></li>
					<li><a href="/ac/fare/index?sortOrder=ASC&sortBy=datetime&filterBy=INCOMING">Réservations</a>			
					<li class="divider"></li>
					<li><a href="$DRIVER_AGENDA_LINK$">Agenda</a></li>
					<li class="divider"></li>					
					<li class="has-dropdown">
						<a href="javascript:void(0)">Sous-traitance</a>
						<ul class="dropdown">
							<li><a href="/">Réserver</a></li>
							<li><a href="/ac/passenger/index">Passagers</a></li>	
							<li><a href="/ac/invoice/index">Factures</a></li>
						</ul>
					</li>
				$elseif(PASSENGER)$	
					<li><a href="/">R&eacute;server</a></li>			
					<li class="divider"></li>
					<li><a href="/ac/fare/index?sortOrder=ASC&sortBy=datetime&filterBy=INCOMING">Mes réservations</a></li>
					<li class="divider"></li>
					<li><a href="/ac/passenger/index">Passagers</a></li>
					<li class="divider"></li>
					<li><a href="$PASSENGER_INVOICES$">Factures</a></li>
				$else$
					<li><a href="">Erreur génération du menu</a></li>	
				$endif$						
			$else$			
				<li>
					<a href="/">R&eacute;server</a>
				</li>
				<li class="divider"></li>			
			$endif$	

          <li class="divider hide-for-small"></li>
        </ul>

        <!-- Right Nav Section -->
        <ul class="right">
          <li class="divider show-for-medium-and-up"></li>
			<li>$AUTH_ACCOUNT_MOBILE$</li>
			<li class="divider"></li>
			<li>$AUTH_LOGIN_MOBILE$</li>
        </ul>
      </section>
    </nav>
	</div>

	<div class="row grad-border hide-for-small">
		<div class="eight columns">
			<a href="/">
				<br/>
				<img src="http://lh3.ggpht.com/b5fjRhJCL8UXJE8wa7FbWpOGAb0m5ka8r3XppIyy2ft2X0MqLynABsYSlRSrJ8yjHIj0wmwAxUN8EDO3pOG2BA=s384" />
			</a>			
		</div>

		<div class="four columns hide-for-small">

			<ul class="inline-list right" id="auth-menu">
			  <li>
			  	$AUTH_ACCOUNT$
			  </li>
			  <li>
			  	$AUTH_LOGIN$
			  </li>
			</ul>			
		</div>
	</div>

	<!--[if IE 7]>    <div class="row grad-border" style="position: relative;z-index:6000"> <![endif]-->
	<!--[if gt IE 7]><!--> <div class="hide-for-small row grad-border"> <!--<![endif]-->	

		<div class="twelve columns">
			<ul class="nav-bar" id="main-menu">

				<li class="show-for-small">$AUTH_ACCOUNT_MOBILE$</li>
				<li class="show-for-small">$AUTH_LOGIN_MOBILE$</li>

			$if(loggedIn)$				
				$if(ALLOCAB)$
					<li class="has-flyout">
						<a href="javascript:void(0)">Utilisateurs</a>
						<a href="#" class="flyout-toggle"><span> </span></a>
						<ul class="flyout">
							<li>
								<a href="/ac/driveraccount/index?sortOrder=DESC&sortBy=createdAt&filterBy=INSPECTION">Chauffeurs</a>
							</li>
							<li>
								<a href="/ac/passengeraccount/index">Passagers</a>
							</li>
							<li>
								<a href="/ac/contact/index">Contacts</a>
							</li>												
						</ul>
					</li>
					<li class="has-flyout">
						<a href="javascript:void(0)">Plateforme</a>
						<a href="#" class="flyout-toggle"><span> </span></a>
						<ul class="flyout">
							<li>																									
								<a href="javascript:search();">Recherche</a>
							</li>								
							<li>
								<a href="/ac/page/index">CMS</a>
							</li>
							<li>
								<a href="/ac/tree/index">Arbres</a>
							</li>
							<li>
								<a href="/ac/shortener/index">Campagnes</a>
							</li>
							<li>
								<a href="/ac/application/index">Applications</a>
							</li>
							<li>
								<a href="/ac/fare/index?sortOrder=ASC&sortBy=datetime&filterBy=INCOMING">Réservations</a>
							</li>											
							<li>
								<a href="/ac/invoice/index">Facturation</a>
							</li>	
							<li>
								<a href="/ac/invoiceresume/index?sortOrder=DESC&sortBy=startDate">Etats chauffeurs</a>
							</li>															
						</ul>
					</li>
					<li class="has-flyout">
						<a href="javascript:void(0)">Véhicules</a>
						<a href="#" class="flyout-toggle"><span> </span></a>
						<ul class="flyout">
							<li>
								<a href="/ac/vehicle/index">Tous les véhicules</a>
							</li>					
							<li>
								<a href="/ac/vehicletype/index">Types de véhicules</a>
							</li>
							<li>
								<a href="/ac/vehiclepanel/index">Gammes de véhicules</a>
							</li>
							<li>
								<a href="/ac/vehiclelicense/index">Licenses de véhicules</a>
							</li>							
						</ul>
					</li>
					<li class="has-flyout">
						<a href="javascript:void(0)">Documents</a>
						<a href="#" class="flyout-toggle"><span> </span></a>
						<ul class="flyout">
							<li>
								<a href="/ac/document/index">Tous les documents</a>
							</li>					
							<li>
								<a href="/ac/documenttype/index">Types de documents</a>
							</li>
							<li>
								<a href="/ac/documenttemplate/index">Modèles</a>
							</li>
						</ul>
					</li>																			
				$elseif(DRIVER)$
					<li><a href="$DRIVER_ACCOUNT_INVOICE_RESUME$">Tableau de bord</a></li>	
					<li><a href="/ac/fare/index?sortOrder=ASC&sortBy=datetime&filterBy=INCOMING">Réservations</a></li>									
					<li><a href="$DRIVER_AGENDA_LINK$">Agenda</a></li>
					<li class="has-flyout">
						<a href="javascript:void(0)">Sous-traitance</a>
						<a href="#" class="flyout-toggle"><span> </span></a>
						<ul class="flyout">
							<li><a href="/">Réserver</a></li>
							<li><a href="/ac/passenger/index">Passagers</a></li>	
							<li><a href="/ac/invoice/index">Factures</a></li>
						</ul>
					</li>
					
				$elseif(PASSENGER)$	
					<li><a href="/">R&eacute;server</a></li>			
					<li><a href="/ac/fare/index?sortOrder=ASC&sortBy=datetime&filterBy=INCOMING">Mes réservations</a></li>
					<li><a href="/ac/passenger/index">Passagers</a></li>
					<li><a href="$PASSENGER_INVOICES$">Factures</a></li>
				$else$
					<li><a href="">Erreur génération du menu</a></li>	
				$endif$						
			$else$			
				<li>
					<a href="/">R&eacute;server</a>
				</li>			
			$endif$											
			</ul>
		</div>
	</div>


	<!--[if IE 7]>
	
	<div class="row">
		<div class="twelve columns"> 
			<div class="alert-box secondary">

				<b>Ce site n'est pas optimisé pour Internet Explorer 7.</b>
				<br/>Merci de mettre à jour votre navigateur pour Internet Explorer 8 ou 9, ou d'utiliser Chrome, Firefox ou safari.
				<br/>
				<em>L'équipe des développeurs chauves d'Allocab, qui se sont trop arrachés les cheveux à coder pour Internet Explorer 7.</em>
				<a href="" class="close">&times;</a>
			</div>  
		</div>
	</div>	

	<![endif]-->	


	<!--[if IE 7]>    <div class="row grad-border" style="position: relative;z-index:4000"> <![endif]-->
	<!--[if gt IE 7]><!--> <div class="row grad-border"> <!--<![endif]-->	
		<div class="twelve columns">
			<div class="row">
				$FLASH_MESSAGE$

				$YIELD_BODY$
			</div>			
		</div>
	</div>



<div id="modal" class="reveal-modal medium">
  <h2 class="title"></h2>
  <p class="content"></p>
  <div class="ok-to-close">
  	<center><input type="button" value="OK" id="close-modal" class="button round"/></center>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>


</body>
</html>
